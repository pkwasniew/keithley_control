# Introduction #

Python script enabling data logging from a KEITHLEY Digital Multimeter/Switch System

## Project description ##

The script was written to provide a simple data logging solution for the 
Keithley 2700 equipped with a 7700 20 channel multiplexer card. It uses 
[ PyVISA ](https://pyvisa.readthedocs.io/en/stable/) to communicate with 
the instrument, connected via a serial-to-USB converter. Commands to the 
instrument are send using the 
[ SCPI ](http://www.tek.com/sites/tek.com/files/media/document/resources/scpiguid.txt)
scripting language, based on the instrument's 
[ user's manual ](http://www.tek.com/keithley-switching-and-data-acquisition-systems/keithley-2700-multimeter-data-acquisition-switch-sys).

The acquired data is streamed into a CSV data file. Optionally, having [ Plotly ](https://plot.ly/) installed and configured,
the temperature readouts can be streamed and plotted live.

## How do I get set up? ##

### Dependencies ###
* [ PyVISA ](https://pyvisa.readthedocs.io/en/stable/)
* [ Pandas ](http://pandas.pydata.org/)
* [ Plotly ](https://plot.ly/) - optional, allows live plotting and streaming. Requires additional setup.

For Windows users - all of the above can be installed using Anaconda. PyVISA requires somewhat special handling:
`conda install -c conda-forge pyvisa `

### Contribution guidelines ###

* There's a lot of room for improvement...

## Quick start ##
For the impatient ones, just type:
```
>python log_temp.py --file temperature_log --com 4 --dt 1 --chan 102 103 104 105 107 108 109 --n 360
```

This will start logging the temperature on all connected channels with a sample taken every second, with 360 samples in total.

## Usage ##

The purpose of the script is to readout temperature values measured by
thermocouples connected to the 7700 multiplexer card inserted into the 
Keithley 2700 multimeter. Running the script without any arguments will
return a help string, describing the input:
```
>python log_temp.py
usage: log_temp.py [-h] [--file FILE] --com COM [--baud BAUD] [--dt DT] --chan CHAN [CHAN ...] [--n N] [--plot PLOT]
```

### Input parameters ###
* `-h`: help - will display the usage message 

* `--file`: define a file name to which the data is to be saved. If no file name is given, the data will be saved into a `temperature_reading_%Y-%m-%d-%H%M%S.xlsx` file in the current directory.

* `--com`: give the com port number to which the Keithley is connected (can 
be checked in Windows device manager)

* `--baud`: com baud rate set in the Keithley - must be correct to allow for proper communication

* `--dt`: time delay in [s] between the samples

* `--chan`: space separated list of channels to be read

* `--n`: total number of samples to be recorded

* `--plot`: boolean (0 or 1, False or True), turns plotting off or on. By default plotting is turned on. 

### Live plotting ###
Plot support requires [ Plotly ](https://plot.ly/) installed and set up for streaming. For details, please check 
the [ Getting Started with Streaming in Python ](https://plot.ly/python/streaming-tutorial/) tutorial.

### Program output ###
Each temperature reading is saved to a CSV file. Independently, the data can be live streamed to a 
plotly plot.

### Who do I talk to? ###

* Repo owner / admin: Pawel Kwasniewski

# -*- coding: utf-8 -*-
# @author: Pawel Kwasniewski
#
# Changes:
#    > Daniel Schuele 11.04.17: 
#      * Added cmd argument parser
#      * Improved data saving 

import visa           # to communicate with the instrument
import time           # to be able to sleep
import datetime       # to produce time stamps
import pandas as pd   # to conveniantly save the data
import sys
import argparse  # command line arguments
import os

try:
    import plotly.plotly as py
    import plotly.graph_objs as go
    import plotly.tools as tls
except ImportError, e:
    if e.message != 'No module named plotly':
        raise


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    if v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


class DataContainer:
    def __init__(self, outFile, channelList, n, plotFlag):
        self.data = {}  # store data in a dictionary first
        self.outFile = outFile  # output file name
        self.plotFlag = plotFlag  # to plot or not to plot
        self.channelList = channelList  # list of channels
        self.n = n  # number of samples collected
        self.s_dict = {}  # stream dictionary for the plot
        
        if self.plotFlag:
            self.plotSetup()
        print('Data will be written to %s' % self.outFile)
        
    def parse(self, rawData):
        """
        Parses the rawData string into the desired parameters.

        :param rawData: @type string
        """
        tmp = rawData[0].split(",")
        tmp = map(lambda x: x.strip("\x11\x13"), tmp)
        values = [tmp[idx*2:(idx+1)*2] for idx in range(len(tmp)/2)]
        timestamp = time.time()
        self.data[timestamp] = {}
        for value in values:
            reading, channel = value
            try:
                temp, unit = reading.split("\x13")
            except ValueError:
                temp = float('nan')
            self.data[timestamp][channel.strip("INTCHAN")] = float(temp)
        self.data = pd.DataFrame(self.data[timestamp], index=self.data.keys())
    
    def appendToFile(self, rawData):
        """
        Appends the currently acquired rawData to the output file.

        :param rawData:
        :return:
        """
        self.parse(rawData)
        if not os.path.isfile(self.outFile):
            with open(self.outFile, 'a') as f:
                self.data.to_csv(f)
        else:
            with open(self.outFile, 'a') as f:
                self.data.to_csv(f, header=False)
        if self.plotFlag:
            self.updatePlot()
        else:
            print(self.data)
        self.cleanDataCont()
        
    def plotSetup(self):
        stream_tokens = tls.get_credentials_file()['stream_ids']
        n_stream_ids = len(stream_tokens)
        n_channels = len(self.channelList)
        if n_stream_ids < n_channels:
            print('Warning!\nNot enough stream ids from plotly to plot the configured number of channels!')
            print('You have %d stream ids, you need at least %d' % (n_stream_ids, n_channels))
            sys.exit(1)
        stream_ids = {}
        pdata = []
        j = 0
        for channel in self.channelList:
            stream_ids[channel] = dict(token=stream_tokens[j],maxpoints=self.n)
            trace = go.Scatter(x = [],y = [],mode = 'lines',name = channel,stream = stream_ids[channel])
            pdata.append(trace)
            j += 1
        layout = go.Layout(
                            title='Live temperature plot',
                            yaxis=dict(title='Temperature [deg C]'),
                            xaxis=dict(title='Time')
                            )
        fig = go.Figure(data=pdata, layout=layout)
        plot_url = py.plot(fig, filename=self.outFile)

        for channel in self.channelList:
            self.s_dict[channel] = py.Stream(stream_id=stream_ids[channel]['token'])
            self.s_dict[channel].open()

    def updatePlot(self):
        for readings in self.data.iteritems():
            chn_no = readings[0]
            ts = readings[1].keys()[0]
            temp = readings[1].values[0]
            self.s_dict[chn_no].write(dict(x=datetime.datetime.fromtimestamp(ts), y=temp))
    
    def closePlotStream(self):
        for key in self.s_dict.keys():
                self.s_dict[key].close()
    
    def cleanDataCont(self):
        self.data = {}


def main():
    # ./keithley2700.py --com=15 --baud=19200 --chan 102 103 104 105 --dt=5 --n=10
    parser = argparse.ArgumentParser(description="measure temperature")
    parser.add_argument("--file", help="file to store data", default="temperature_reading", required=False)
    parser.add_argument("--com", type=int, help="COM interface", required=True)
    parser.add_argument("--baud", type=int, help="COM baud rate", required=False, default=9600)
    parser.add_argument("--dt", help="sample delay in seconds", type=float, default=10)
    parser.add_argument("--chan", help="temperature channels", nargs="+", required=True)
    parser.add_argument("--n", type=int, help="samples total", default=-1)
    parser.add_argument("--plot", type=str2bool, help="to plot or not to plot...", default=True, required=False)
    args = parser.parse_args(sys.argv[1:])

    filename = args.file
    plotFlag = args.plot
    com = "com%i" % args.com
    baud = args.baud
    dt = args.dt
    channels = args.chan
    numchannels = len(args.chan)
    listchannels = ",".join(args.chan)
    n = args.n

    cmds = (
            "*RST",  # restore defaults
            "TRAC:CLE",  # clear buffer
            "INIT:CONT OFF",  # disable continuous initiation
            "FORM:DATA ASC",  # change data format to ASCII
            "FORM:ELEM READ, UNIT, CHAN",  # display value, unit, time stamp, channel number
            "SENS:FUNC 'TEMP', (@%s)" % listchannels,
            "SENS:TEMP:TC:RJUN:RSEL INT",  # Select internal refernce junction
            "SAMP:COUN %i" % numchannels,   # set sample count (how many channels to be read)
            "ROUT:SCAN (@%s)" % listchannels,  # specify list of channels to be scanned
            "ROUT:SCAN:TSO IMM",  # start scan immediately after trigger
            "ROUT:SCAN:LSEL INT",  # enable scan
            )
    
    # Open connection to the instrument
    rm = visa.ResourceManager()
    kth = rm.open_resource(com)
    kth.baud_rate = baud
    kth.timeout = 10000
    kth.read_termination = "\r"
    kth.write_termination = "\r"
    
    # Initialize the instrument
    for cmd in cmds:
        kth.write(cmd)
    # Setup the streaming plot
    # channel_setup = pd.read_excel("./Channel_setup.xlsx",index_col=0)
    out_file = '%s_%s.csv' % (filename, time.strftime("%Y-%m-%d-%H%M%S"))
    dcont = DataContainer(out_file, listchannels.split(','), n, plotFlag)  # initialize data container
    while True:
        if n == 0:
            break
        n -= 1
        try:
            raw_data = kth.query_ascii_values("READ?", separator="!!", converter="s")
            dcont.appendToFile(raw_data)
            time.sleep(dt)
        except KeyboardInterrupt:
            dcont.closePlotStream()
            kth.write("ROUT:SCAN:LSEL NONE")  # Disable scan
            kth.close()  # Close the connection to the instrument
    dcont.closePlotStream()
    print('Finished!')
    kth.write("ROUT:SCAN:LSEL NONE")  # Disable scan
    kth.close()  # Close the connection to the instrument


# call main
if __name__ == '__main__':
    main()
